let http = require("http");

// Mock Database
let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@gmail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@gmail.com"
	}
];

http.createServer(function(request, response) {
	// Route for returning all items
	if (request.url == "/users" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'application/json'});

		response.write(JSON.stringify(directory));

		response.end();
	}

	// Route for creating data
	if (request.url == "/users" && request.method == "POST"){
		let requestBody = '';

		request.on('data', function(data){

			// requestBody = requestBody + data;
			requestBody += data;
		});

		// response end step - this request will only runs after the request has completely been sent.
		request.on('end', function(){
			// check if at this point the requestBody is of data type STRING
			console.log(typeof requestBody);
			
			// Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			// Create a new object representing the new mock database record
			let newUser = {
				"name" : requestBody.name,
				"email" :  requestBody.email
			}

			// Add the new user into the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(newUser));
			response.end();
		});
	}
}).listen(4000);

console.log('Server running at localhost:4000');
