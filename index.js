let http = require("http");

http.createServer(function(request, response) {
	if(request.url == "/items" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data retrieved from the database');
	}

	// POST request to create/add information
	if(request.url == "/items" && request.method == "POST") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be sent to database');
	}
}).listen(4000);

console.log('Server running at localhost:4000');

